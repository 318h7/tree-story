default:
	g++ src/game.cpp -o build/treeStory.exe -O2 -Wall -Wno-missing-braces -I include/ -L lib/ -lraylib -lopengl32 -lgdi32 -lwinmm
	xcopy resources build\resources\ /E/H/C/I/Y/S/D

production:
	g++ src/game.cpp -o build/treeStory.exe -mwindows -O2 -Wall -Wno-missing-braces -I include/ -L lib/ -lraylib -lopengl32 -lgdi32 -lwinmm
	xcopy resources build\resources\ /E/H/C/I/Y/S/D