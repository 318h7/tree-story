#include <string>
#include <unordered_map>
#include <raymath.h>
#include "../Sprite/AnimatedSprite.cpp"
#include "../StateMachine/StateMachine.cpp"

using namespace std;

typedef enum Direction
{
    LEFT,
    RIGHT,
} Direction;

class Player {
  private:
    Vector2 position;
    AnimatedSprite sprite;
    StateMachine state;
    PhysicsBody body;
    Direction direction;
  public:
    Player();
    Player(Vector2 startPosition, string spriteName);
    void update();
    void draw();
    void init();
    void setPosition(Vector2 newPosition);
    Vector2 getPosition();
    void dispatch(string action);
    void destroy();
    PhysicsBody getBody();
    void setDirection(Direction newDirection);
    void move();
};
