#include "Player.h"
#include <iostream>

Rectangle RectangleScale(Rectangle rec, float xScale, float yScale)
{ 
    return (Rectangle){rec.x, rec.y, rec.width * xScale, rec.height * yScale};
}

void DrawSprite(Vector2 pos, Rectangle spriteRec, bool flipX, bool flipY, Texture2D spriteAtlas) 
{
    DrawTextureRec(
        spriteAtlas,
        RectangleScale(spriteRec, flipX == true ? -1 : 1, flipY == true ? -1 : 1),
        pos,
        WHITE
    );
}

#define VELOCITY 0.15f

Player::Player(){
    position = { 0.0f, 0.0f };
    direction = RIGHT;
};

Player::Player(Vector2 startPosition, string spriteName){
    position = startPosition;

    unordered_map<string, Action> actions;

    actions["run"] = {0, 5, 10, Forward};
    actions["goToSleep"] = {6, 13, 8, Once};
    actions["sleep"] = {14, 18, 4, PingPong};
    actions["wakeUp"] = {6, 13, 8, BackwardOnce};
    actions["lookAround"] = {20, 30, 10, Once};
    actions["sitDown"] = {20, 30, 10, BackwardOnce};
    actions["idle"] = {19, 19, 10, Still};

    sprite = AnimatedSprite(spriteName, 60, actions); 
    state = StateMachine();
    Vector2 size = sprite.getSize();
    body = CreatePhysicsBodyRectangle(
        Vector2Add(startPosition, Vector2Divide(size, (Vector2){2.0f, 2.0f})),
        sprite.getSize().x,
        sprite.getSize().y,
        1.0f
    );
    // Constrain body rotation to avoid little collision torque amounts
    body->freezeOrient = true;
};

void Player::init() {
    state.addState("idle");
    state.addState("run");
    state.addState("lay");
    state.addState("sleep");
    state.addState("getup");
    state.addState("look");
    state.addState("sit");

    state.addStateAction("run", "idle", [this](function<void(string)>setState){
        setState("idle");
        sprite.setAction("idle");
    });

    state.addStateAction("idle", "run", [this](function<void(string)>setState){
        setState("run");
        sprite.setAction("run");
    });

    state.addStateAction("idle", "look", [this](function<void(string)>setState){
        setState("look");
        sprite.setAction("lookAround");
    });

    state.addStateAction("look", "sit", [this](function<void(string)>setState){
        setState("sit");
        sprite.setAction("sitDown");
    });

    state.addStateAction("sit", "idle", [this](function<void(string)>setState){
        setState("idle");
        sprite.setAction("idle");
    });

    state.addStateAction("idle", "sleep", [this](function<void(string)>setState){
        setState("lay");
        sprite.setAction("goToSleep");
    });

    state.addStateAction("lay", "sleep", [this](function<void(string)>setState){
        setState("sleep");
        sprite.setAction("sleep");
    });

    state.addStateAction("sleep", "wakeup", [this](function<void(string)>setState){
        setState("getup");
        sprite.setAction("wakeUp");
    });

    state.addStateAction("getup", "idle", [this](function<void(string)>setState){
        setState("idle");
        sprite.setAction("idle");
    });
}

void Player::update(){
    if (state.isState("look") && sprite.hasFinished("lookAround")) {
        state.dispatch("sit");
    }
    if (state.isState("sit") && sprite.hasFinished("sitDown")) {
        state.dispatch("idle");
    }

    if (state.isState("lay") && sprite.hasFinished("goToSleep")) {
        state.dispatch("sleep");
    }
    if (state.isState("getup") && sprite.hasFinished("wakeUp")) {
        state.dispatch("idle");
    }

    if (state.isState("look") && sprite.hasFinished("lookAround")) {
        state.dispatch("sit");
    }
    if (state.isState("sit") && sprite.hasFinished("sitDown")) {
        state.dispatch("idle");
    }
    sprite.update();
};

void Player::draw(){
    Vector2 size = sprite.getSize();
    Vector2 bodyPos = body->position;
    DrawSprite(
        Vector2Subtract(
            bodyPos,
            Vector2Divide(size, (Vector2){2.0, 2.0})
        ),
        sprite.getFrame(),
        direction == RIGHT,
        false,
        sprite.getTexture()
    );
    //DrawTextureRec(sprite.getTexture(), sprite.getFrame(), position, WHITE);
};

void Player::setPosition(Vector2 newPosition){
    position = newPosition;
};

Vector2 Player::getPosition(){
    return position;
};

void Player::dispatch(string action) {
    state.dispatch(action);
};

void Player::setDirection(Direction newDirection) {
    direction = newDirection;
}

void Player::destroy() {
    sprite.destroy();
}

void Player::move(){
    body->velocity.x = direction == RIGHT ? VELOCITY : -VELOCITY;
}

PhysicsBody Player::getBody() {
    return body;
}

