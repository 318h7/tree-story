#include <string>
#include <unordered_map>
#include <vector>
#include <functional>
#include <algorithm>

using namespace std;

const string DEFAULT_STATE = "none";

class StateMachine {
  private:
    string state;
    unordered_map<
      string,
      unordered_map<
        string,
        function<void(function<void(string name)> setState)>
      >
    > stateRules;
    void setState(string newState);
  public:           
    StateMachine();
    string getState();
    bool isState(string name);
    void addState(string stateName);
    void addStateAction(string state, string actionName, function<void(function<void(string name)> setState)> callback);
    void dispatch(string action);
};
