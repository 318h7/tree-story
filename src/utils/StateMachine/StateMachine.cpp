#include "StateMachine.h"
#include <iostream>

StateMachine::StateMachine()
{
    state = DEFAULT_STATE;
}

void StateMachine::setState(string newState) {
    if (stateRules.find(newState) != stateRules.end()) {
        state = newState;
    }
};

string StateMachine::getState(){
    return state;
};

void StateMachine::addState(string stateName){
    stateRules.insert(make_pair(
        stateName,
        unordered_map<string, function<void(function<void(string name)> setState)>>()
    ));

   if (!state.compare(DEFAULT_STATE)) {
       state = stateName;
   }
};

bool StateMachine::isState(string stateName){
   if (!state.compare(stateName)) {
       return true;
   } else {
       return false;
   }
};


void StateMachine::addStateAction(
    string state,
    string actionName,
    function<void(function<void(string name)> setState)> callback
){
    stateRules[state][actionName] = callback;
};

void StateMachine::dispatch(string action){
    function<void(function<void(string name)> setState)> callback = stateRules[state][action];

    if (callback) {
      callback([this](string name){
          setState(name);
      });
    }
};
