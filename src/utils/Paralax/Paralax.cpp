#include "Paralax.h"

Paralax::Paralax() {
    scrollingBack = 0.0f;
    scrollingMid = 0.0f;
    scrollingFore = 0.0f;
}

Paralax::Paralax(string bg, string mg, string fg) {
    background = LoadTexture(string(RESOURCES).append(bg).c_str());
    midground = LoadTexture(string(RESOURCES).append(mg).c_str());
    foreground = LoadTexture(string(RESOURCES).append(fg).c_str());
    SetTextureFilter(background, TEXTURE_FILTER_BILINEAR); // Makes the texture smoother when upscaled

    scrollingBack = 0.0f;
    scrollingMid = 0.0f;
    scrollingFore = 0.0f;
}

void Paralax::update()
{
    // Update
    scrollingBack -= 0.1f;
    scrollingMid -= 0.5f;
    scrollingFore -= 1.0f;

    // NOTE: Texture is scaled twice its size, so it should be considered on scrolling
    if (scrollingBack <= -background.width*2) scrollingBack = 0;
    if (scrollingMid <= -midground.width*2) scrollingMid = 0;
    if (scrollingFore <= -foreground.width*2) scrollingFore = 0;
}

void Paralax::draw()
{
    // Draw background image twice
    // NOTE: Texture is scaled twice its size
    DrawTextureEx(background, (Vector2){ scrollingBack, 20 }, 0.0f, 2.0f, WHITE);
    DrawTextureEx(background, (Vector2){ background.width*2 + scrollingBack, 20 }, 0.0f, 2.0f, WHITE);

    // Draw midground image twice
    DrawTextureEx(midground, (Vector2){ scrollingMid, 20 }, 0.0f, 2.0f, WHITE);
    DrawTextureEx(midground, (Vector2){ midground.width*2 + scrollingMid, 20 }, 0.0f, 2.0f, WHITE);

    // Draw foreground image twice
    DrawTextureEx(foreground, (Vector2){ scrollingFore, 70 }, 0.0f, 2.0f, WHITE);
    DrawTextureEx(foreground, (Vector2){ foreground.width*2 + scrollingFore, 70 }, 0.0f, 2.0f, WHITE);
}

void Paralax::destroy() {
    UnloadTexture(background);  // Unload background texture
    UnloadTexture(midground);   // Unload midground texture
    UnloadTexture(foreground);  // Unload foreground texture
}