#include <string>
#include <cmath>

#include "raylib.h"

#include "../constants.h"

using namespace std;

class Paralax {
  private:
    Texture2D background, midground, foreground;
    float scrollingBack, scrollingMid, scrollingFore;

    Rectangle frame;
    std::string resourceFolder;
    int currentFrame, framesCounter, speed, frames;

  public:
    Paralax();         
    Paralax(string bg, string mg, string fg);
    void update();
    void draw();
    void destroy();
};
