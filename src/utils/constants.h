#if !defined(CONSTANTS_H)
#define CONSTANTS_H 1

#include <string>

using namespace std;

const string RESOURCES = "resources/";
const int TARGET_FPS = 60;

#endif