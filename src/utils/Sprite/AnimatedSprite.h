#include <string>
#include <unordered_map>

#include "../constants.h"

enum Animation { Forward, Backward, PingPong, Once, BackwardOnce, Still };

typedef struct Action {
    int startFrame;
    int endFrame;
    int speed;
    Animation animationType;
} Action;

class AnimatedSprite {
  private:
    Texture2D texture;
    Rectangle frame;
    std::string resourceFolder;
    int currentFrame, framesCounter;
    unordered_map<string, Action> actions;
    std::string currentAction;
    bool finished;
    int delta;
    void reset();
    void updateDelta();

  public:           
    AnimatedSprite(std::string name, int frameWidth, unordered_map<string, Action> actions);
    AnimatedSprite();
    void update();
    Rectangle getFrame();
    Texture2D getTexture();
    void setAction(std::string name);
    void destroy();
    bool hasFinished(std::string name);
    string getAction();
    Vector2 getSize();
};
