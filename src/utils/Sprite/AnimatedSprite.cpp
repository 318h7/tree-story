#include "AnimatedSprite.h"

using namespace std;


AnimatedSprite::AnimatedSprite() {
    frame = {0.0f, 0.0f, 0.0f, 0.0f};
    currentFrame = 0;
    framesCounter = 0;
    finished = false;   
}

AnimatedSprite::AnimatedSprite(string name, int frameWidth, unordered_map<string, Action> actionsMap) {
    actions = actionsMap;
    currentAction = actions.begin()->first;
    currentFrame = actions[currentAction].startFrame;
    framesCounter = 0;
    texture = LoadTexture(string(RESOURCES).append(name).c_str());
    frame = {0.0f, 0.0f, (float)frameWidth,  (float)texture.height};
    updateDelta();
}

Vector2 AnimatedSprite::getSize() {
    return {frame.width, frame.height};
}

void AnimatedSprite::update()
{
    framesCounter++;
    if (framesCounter >= (TARGET_FPS / actions[currentAction].speed))
    {
        framesCounter = 0;
        currentFrame += delta;

        updateDelta();
        reset();
        
        frame.x = (float)currentFrame * frame.width;
    }
}

void AnimatedSprite::reset() {      
    switch (actions[currentAction].animationType)
    {
        case Animation::Still:
            if (currentFrame != actions[currentAction].startFrame) {
                currentFrame = actions[currentAction].startFrame;
            }
            break;
        case Animation::Forward:
            if (currentFrame > actions[currentAction].endFrame) {
                currentFrame = actions[currentAction].startFrame;
            }
            break;
        case Animation::Once:
            if (currentFrame > actions[currentAction].endFrame) {
                currentFrame = actions[currentAction].endFrame;
                finished = true;
            }
            break;
        case Animation::Backward:
            if (currentFrame < actions[currentAction].startFrame) {
                currentFrame = actions[currentAction].endFrame;
            }
            break;
        case Animation::BackwardOnce:
            if (currentFrame < actions[currentAction].startFrame) {
                currentFrame = actions[currentAction].startFrame;
                finished = true;
            }
            break;
        case Animation::PingPong:
            if (currentFrame > actions[currentAction].endFrame) {
                currentFrame = actions[currentAction].endFrame;
            } else if (currentFrame < actions[currentAction].startFrame) {
                currentFrame = actions[currentAction].startFrame;
            }
            break;
        default:
            break;

    }
}

void AnimatedSprite::updateDelta() {
    switch (actions[currentAction].animationType)
    {
        case Animation::Still:
            delta = 0;
        case Animation::Forward:
        case Animation::Once:
            delta = 1;
            break;
        case Animation::Backward:
        case Animation::BackwardOnce:
            delta = -1;
            break;
        case Animation::PingPong:
            if (currentFrame > actions[currentAction].endFrame) {
                delta = -1;
            } else if (currentFrame < actions[currentAction].startFrame) {
                delta = 1;
            }
            break;
        default:
            delta = 1;
            break;
    }
}

void AnimatedSprite::setAction(string name) {
    currentAction = name;
    finished = false;   
    framesCounter = 0;
    if (actions[currentAction].animationType == Backward || actions[currentAction].animationType == BackwardOnce) {
        currentFrame = actions[currentAction].endFrame;
    } else {
        currentFrame = actions[currentAction].startFrame;
    }

    updateDelta();
}

Rectangle AnimatedSprite::getFrame()
{
    return frame;
}

Texture2D AnimatedSprite::getTexture()
{
    return texture;
}

void AnimatedSprite::destroy() {
    UnloadTexture(texture);
};


bool AnimatedSprite::hasFinished(string name) {
    if (!currentAction.compare(name)) {
        return finished;
    }
    return false;
}

string AnimatedSprite::getAction() {
    return currentAction;
}

