
#include "raylib.h"
#define PHYSAC_IMPLEMENTATION
#include "physac.h"

#include "utils/Player/Player.cpp"
#include "utils/Paralax/Paralax.cpp"
#include "utils/constants.h"
#include <iostream>

typedef enum GameScreen
{
    MENU = 0,
    GAMEPLAY,
    ENDING
} GameScreen;

GameScreen currentScreen = MENU;


void updateScene()
{
    switch (currentScreen)
    {
    case MENU:
    {
        if (IsKeyPressed(KEY_ENTER) || IsGestureDetected(GESTURE_TAP))
        {
            currentScreen = GAMEPLAY;
        }
    }
    break;
    case GAMEPLAY:
    {
        if (IsKeyPressed(KEY_ENTER) || IsGestureDetected(GESTURE_TAP))
        {
            currentScreen = ENDING;
        }
    }
    break;
    case ENDING:
    {
        if (IsKeyPressed(KEY_ENTER) || IsGestureDetected(GESTURE_TAP))
        {
            currentScreen = MENU;
        }
    }
    break;
    default:
        break;
    }
}

void debugPhysics()
{
    int bodiesCount = GetPhysicsBodiesCount();

    for (int i = 0; i < bodiesCount; i++)
    {
        PhysicsBody body = GetPhysicsBody(i);

        int vertexCount = GetPhysicsShapeVerticesCount(i);
        for (int j = 0; j < vertexCount; j++)
        {
            // Get physics bodies shape vertices to draw lines
            // Note: GetPhysicsShapeVertex() already calculates rotation transformations
            Vector2 vertexA = GetPhysicsShapeVertex(body, j);

            int jj = (((j + 1) < vertexCount) ? (j + 1) : 0);   // Get next vertex or first to close the shape
            Vector2 vertexB = GetPhysicsShapeVertex(body, jj);

            DrawLineV(vertexA, vertexB, GREEN);     // Draw a line between two vertex positions
        }
    }
}

int main(void)
{
    Paralax bg;
    Player player;
    const int screenWidth = 800;
    const int screenHeight = 450;
    
    InitPhysics();

    PhysicsBody floor = CreatePhysicsBodyRectangle((Vector2){ screenWidth/2.0f, (float)screenHeight }, (float)screenWidth, 100, 10);
    floor->enabled = false;

    InitWindow(screenWidth, screenHeight, "Tree story - a life of a tree");

    bg = Paralax(
        "cyberpunk_street_background.png",
        "cyberpunk_street_midground.png",
        "cyberpunk_street_foreground.png"
    );
    player = Player({ 350.0f, 10.0f }, "bunny.png");
    player.init();

    SetTargetFPS(TARGET_FPS);

    while (!WindowShouldClose()) // Detect window close button or ESC key
    {
        if (IsKeyDown(KEY_RIGHT)) {
            player.dispatch("run");
            player.setDirection(RIGHT);
            player.move();
        }
        if (IsKeyReleased(KEY_RIGHT)) {
            player.dispatch("idle");
        }

        if (IsKeyDown(KEY_LEFT)) {
            player.dispatch("run");
            player.setDirection(LEFT);
            player.move();
        }
        if (IsKeyReleased(KEY_LEFT)) {
            player.dispatch("idle");
        }

        updateScene();
        UpdatePhysics();

        bg.update();
        player.update();
        
        BeginDrawing();
 
        ClearBackground(RAYWHITE);

        bg.draw();
        player.draw();

        switch (currentScreen)
        {
        case MENU:
        {
            DrawText("TITLE SCREEN", 20, 20, 40, DARKGREEN);
        }
        break;
        case GAMEPLAY:
        {
            // if (IsKeyReleased(KEY_I)) player.dispatch("idle");
            // if (IsKeyReleased(KEY_R)) player.dispatch("run");
            // if (IsKeyReleased(KEY_S)) player.dispatch("sleep");
            // if (IsKeyReleased(KEY_W)) player.dispatch("wakeup"); 
            // if (IsKeyReleased(KEY_L)) player.dispatch("look");

            DrawText("GAMEPLAY SCREEN", 20, 20, 40, MAROON);
        }
        break;
        case ENDING:
        {
            DrawText("ENDING SCREEN", 20, 20, 40, DARKBLUE);
        }
        break;
        default:
            break;
        }

        //debugPhysics();
        EndDrawing();
    }

    // De-Initialization
    player.destroy();
    bg.destroy();
    ClosePhysics();
    CloseWindow();

    return 0;
}
